#! /usr/bin/env python3
# encoding:utf-8

from prettytable import PrettyTable
import requests
import urllib3
urllib3.disable_warnings()

class TrainCollection(object):

    header = u"车次 始发\终点站 出发\到达站 出发\到达时间 历时 商务座 一等座 二等座 软卧 硬卧 软座 硬座 无座".split()

    def __init__(self, rows,station):
        self.rows = rows
        self.station = station
    def colored(self, color, text):
        table = {
            'red': '\033[91m',
            'green': '\033[92m',
            # no color
            'nc': '\033[0m'
        }
        cv = table.get(color)
        nc = table.get('nc')
        return ''.join([cv, text, nc])
    def getPrice(self,row):
        date=row[13][:4]+'-'+row[13][4:6]+'-'+row[13][6:]
        url='https://kyfw.12306.cn/otn/leftTicket/queryTicketPrice?train_no={}&from_station_no={}&to_station_no={}&seat_types={}&train_date={}'.format(
            row[2],row[16],row[17],row[35],date)
        r = requests.get(url, verify=False)
        return r.json()['data']
    @property
    def trains(self):
        for row in self.rows:
            data = row.split('|')
            # price=self.getPrice(data)
            # seats=('A9','M','O','A4','A3','A2','A1','A1')
            # prices=[]
            # for i in seats:
            #     if i in price:
            #         prices.append(price[i])
            #     else:
            #         prices.append('')
            sp = []
            for i,j in enumerate([32,31,30,23,28,24,29,26]):
                if data[j] != '无':
                    # tem = '\n'.join([self.colored('green', data[j]),self.colored('red', prices[i])])
                    tem = self.colored('green', data[j])
                else:
                    # tem = '\n'.join([data[j],prices[i]])
                    tem = data[j]
                sp.append(tem)
            train = [
               
                data[3],     # 车次
                '\n'.join([self.colored('green', self.station[data[4]]),self.colored('red', self.station[data[5]])]),   # 始发、终点站
                '\n'.join([self.colored('green', self.station[data[6]]),self.colored('red', self.station[data[7]])]),   # 出发、到达站
                '\n'.join([self.colored('green', data[8]),self.colored('red', data[9])]),    # 出发、到达时间
                data[10],   #历时
            ]
            train += sp
            yield train

    def pretty_print(self):
        """
        数据已经获取到了,剩下的就是提取我们要的信息并将它显示出来。
        `prettytable`这个库可以让我们它像MySQL数据库那样格式化显示数据。
        """
        pt = PrettyTable()
        # 设置每一列的标题
        pt._set_field_names(self.header)
        for train in self.trains:
            pt.add_row(train)
        print(pt)
